from trytond.pool import Pool
from .stock import *
from .product import *


def register():
    Pool.register(
        Template,
        Product,
        Configuration,
        Location,
        Move,
        LocationSpaceAvailability,
        module='stock_storage_space', type_='model')
    Pool.register(
        OpenLocationSpaceAvailability,
        module='stock_storage_space', type_='wizard')